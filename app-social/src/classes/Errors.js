export default class {

  constructor() {
    this.errors = { };
  }

  get(field) {
    if(this.errors[field])
    {
      return this.errors[field];
    }
  }

  has(field) {
    return this.errors.hasOwnProperty(field);
  }

  record(errors) {
    this.errors = errors;
  }

  clear(field) {
    console.log('hit >>>', field);
    delete this.errors[field];
  }

  any() {
    return Object.keys(this.errors).length > 0;
  }

}
