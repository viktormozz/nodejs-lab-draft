// vue.config.js
module.exports = {
  css: {
    loaderOptions: {
      // pass options to sass-loader
      sass: {
        // @/ is an alias to src/
        // so this assumes you have a file named `src/variables.scss`
        prependData: `
          @import "./node_modules/bulma/bulma.sass";
          @import "@/assets/sass/_variables.scss";
          `
      }
    }
  }
}
