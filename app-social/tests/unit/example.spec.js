import { shallowMount } from '@vue/test-utils';
import LoginForm from '@/components/LoginForm.vue';
import RegisterForm from '@/components/RegisterForm.vue';

describe('LoginForm.vue', () => {
  it('renders props.msg when passed', () => {
    const title = 'new message';
    const wrapper = shallowMount(LoginForm, {
      propsData: { title },
    });
    expect(wrapper.text()).toMatch(title);
  });
});
describe('RegisterForm.vue', () => {
  it('renders props.msg when passed', () => {
    const title = 'new message';
    const wrapper = shallowMount(RegisterForm, {
      propsData: { title },
    });
    expect(wrapper.text()).toMatch(title);
  });
});
