const express = require('express');
const jwt = require('jsonwebtoken'); 
const fs = require('fs');
const cors = require('cors');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');

const MongoClient = require('mongodb').MongoClient

const mongoParams = {
    link: 0,
    url: 'mongodb://127.0.0.1:27017',
    dbName: 'laboratoria',
    collection: 'users' 
}

const app = express();
const app_config = {
    'port': 3000
}

const corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

const re_email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const re_password = /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/;


app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/', (req, res) => res.send('Welcome, there is nothing to do here!') );

app.get('/get_userwall', isAuth, (req, res) => {

    res.json({ 'message': 'Welcome' });

});

app.post('/register', cors(corsOptions), async (req, res) => {

    let errors = validateFrontRegister(req.body.firstname, req.body.lastname, req.body.email, req.body.password, req.body.cpassword);

    if(Object.keys(errors).length)
    {
        console.log('not data to check!');
        res.status(200).json({ 
            'statusCode': 250,
            'errors': errors
        });
    }
    else
    {
        let checkUserExists = await checkIfUserExists();

        if(checkUserExists.error)
        {
            res.status(200).json({ 
                'statusCode': 250,
                'errors': { email: 'Another user exists with this email' }
            });
        }
        else
        {

            // bcrypt.hash(req.body.password, 10, function (err, hash) {


            // });
        }   

    }

});

app.post('/login', cors(corsOptions), async (req, res, next) => {

    let errors = validateFrontLogin(req.body.email, req.body.password);

    if(Object.keys(errors).length)
    {
        console.log('not data to check!');
        res.status(200).json({ 
            'statusCode': 250,
            'errors': errors
        });
    }
    else
    {
        try {
            MongoClient.connect(mongoParams.url, { useNewUrlParser: true }, (err, client) => {

                const dbo = client.db(mongoParams.dbName)
    
                //Step 1: declare promise
                var mongoPromise = () => {
                    return new Promise((resolve, reject) => {
                       dbo
                        .collection('users')
                        .find({email: req.body.email})
                        .limit(1)
                        .toArray(function(err, data) {
                            err 
                               ? reject(err) 
                               : resolve(data[0]);
                          });
                    });
                };            
    
                //Step 2: async promise handler
                var callmongoPromise = async () => {
                        
                    var result = await (mongoPromise());
                    //anything here is executed after result is resolved
                    return result;
                };
    
                //Step 3: make the call
                callmongoPromise().then(function(result) {
                    client.close();
                    let token = generateToken(result);
                    res.status(200).json({ 'token': token, 'data': result });
                });            
    
            });
        } catch(e) {
            next(e);
        }
    
    }

});

app.listen( app_config.port, () => console.log(`App started at ${app_config.port}`) )

function generateToken(result)
{
    let payload = {
        id: result.id,
        firstname: result.firstname,
        lastname: result.firstname,
        email: result.email
    },
    signOptions = {
        issuer:  result.firstname,
        subject:  result.email,
        audience:  '',
        expiresIn:  "5m",
        algorithm:  "HS256"
    },

    privateKey = fs.readFileSync('./keys/privatekey.pem', 'utf8'),
    publicKey = fs.readFileSync('./keys/publickey.pem', 'utf8'),

    token = jwt.sign(payload, privateKey, signOptions);

    return token;

}

function validateFrontLogin(email, password)
{
    let errors = { };

    if(!re_email.test(email))
    {
        errors.email = "Invalid email";
    }
    if(!re_password.test(password))
    {
        errors.password = "Invalid password";
    } 

    return errors;

}

function validateFrontRegister(firstname, lastname, email, password, cpassword)
{
    let errors = { };

    if(firstname.length < 2)
    {
        errors.firstname = "Firstname is required";
    }
    if(lastname.length < 2)
    {
        errors.lastname = "Lastname is required";
    }
    if(!re_email.test(email))
    {
        errors.email = "Invalid email";
    }
    if(!re_password.test(password))
    {
        errors.password = "Invalid password";
    } 
    if(password != cpassword)
    {
        errors.cpassword = "Your confirm password does not match";
    } 

    return errors;

}


function isAuth(req, res, next)
{
    if( typeof req.headers.authorization !== 'undefined' )
    {
        let token = req.headers.authorization.split(' ')[1],
            privateKey = fs.readFileSync('./keys/privatekey.pem', 'utf8');

        jwt.verify( token, privateKey, { algorithm: 'HS256'}, (err, decoded) => {
            if( err )
            {
                res.status(500).json({ error: 'Restricted' })
            }

            console.log(decoded);

            return next();
        });

    } 
    else
    {
        res.status(500).json({ error: 'Restricted' })
    }
}

async function checkIfUserExists(email)
{

    return { error: 'default error' };

}